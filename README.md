# Protoboard Layout Tool in SVG / Python

This is a simple tool to create protoboard layouts in SVG format.

## Example

![Protoboard wiring Tool in SVG](wiring.svg)