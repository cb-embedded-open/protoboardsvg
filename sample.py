from wiring import Protoboard

board = Protoboard(28, 7, "#2A2A2A")

board.resistor(9, 3, 5, 0)
board.wire(2, 4, 5, -2, color="#b19319")
board.wire(8, 2, 7, 0, color="#0e693a")
board.wire(2, 5, 6, 0, color="#bfc8c3")
board.wire(7, 6, 2, 0, color="#2b2d98")
board.resistor(2, 6, 4, 0)
board.dip8(10, 4)
board.wire(9, 4, 5, 2, color="#b13519")
board.wire(9, 5, 5, 2, color="#b13519")
board.wire(14, 4, 2, 0, color="#2b2d98")
board.wire(14, 5, 2, 0, color="#2b2d98")
board.wire(15, 6, 1, 0, color="#666666")
board.wire(15, 7, 1, 0, color="#666666")
board.wire(9, 8, 7, 0, color="#aea9a9")
board.wire(2, 8, 7, -1, color="#2b2d98")
board.capacitor(2, 7, 4, 0)

board.save('mon_protoboard.svg')