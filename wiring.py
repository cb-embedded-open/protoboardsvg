class Protoboard:
    def __init__(self, width, height, color = '#45220b'):
        self.width = width
        self.height = height
        self.elements = ''
        self.board_color = color

    def generate_svg(self):
        return f"""
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 {self.width+2} {self.height+2}">
    <defs>
        <pattern id="padPattern" patternUnits="userSpaceOnUse" width="1" height="1">
            <rect x="0" y="0" width="1" height="1" fill="{self.board_color}" />
            <circle cx=".5" cy=".5" r="0.35" fill="#76592f" />
            <circle cx=".5" cy=".5" r="0.15" fill="#000000" />
        </pattern>

        <style>
            svg {{ overflow: visible; }}
            .dip8 {{ transform-origin: 1 1; }}
        </style>

        <linearGradient id="gradientResistor" x1="0" y1="0" x2="0" y2="1">
            <stop offset="-10%" stop-color="#022f66" />
            <stop offset="50%" stop-color="#0088ff" />
            <stop offset="110%" stop-color="#022f66" />
        </linearGradient>

        <linearGradient id="gradientCapacitor" x1="0" y1="0" x2="0" y2="1">
            <stop offset="-10%" stop-color="#765220" />
            <stop offset="50%" stop-color="#e3ae3b" />
            <stop offset="110%" stop-color="#765220" />
        </linearGradient>

        <linearGradient id="gradientWire" x1="0" y1="0" x2="0" y2="1">
            <stop offset="-10%" stop-color="#37064d" />
            <stop offset="50%" stop-color="#6f1195" />
            <stop offset="110%" stop-color="#330657" />
        </linearGradient>

        <marker id="resistorMarker" markerWidth="5" markerHeight="2" refX="2.5" refY="1" orient="auto" overflow="visible">
            <rect x="0" y="0" width="5" height="2" fill="url(#gradientResistor)" rx="0.5" ry="1" stroke="#000000" stroke-width="0.2"/>
        </marker>

        <marker id="wireMarker" markerWidth="1" markerHeight="1" refX="0" refY="0.5" orient="auto" overflow="visible" markerUnits="userSpaceOnUse">
            <rect x="0" y="0" width="10%" height="1" fill="url(#gradientWire)" rx="0.5" ry="1" stroke="#000000" stroke-width="0.2"/>
        </marker>

        <marker id="capacitorMarker" markerWidth="3.5" markerHeight="1.5" refX="1.75" refY="0.75" orient="auto" overflow="visible">
            <rect x="0" y="0" width="3.5" height="1.5" fill="url(#gradientCapacitor)" rx="3" ry="2" stroke="#000000" stroke-width="0.2"/>
        </marker>

        <g id="dip8" class="dip8">
            <g fill="grey">            
                <circle cx="0" cy="0" r="0.25" />
                <circle cx="0" cy="1" r="0.25" />
                <circle cx="0" cy="2" r="0.25" />
                <circle cx="0" cy="3" r="0.25" />

                <circle cx="3" cy="0" r="0.25" />
                <circle cx="3" cy="1" r="0.25" />
                <circle cx="3" cy="2" r="0.25" />
                <circle cx="3" cy="3" r="0.25" />
            </g>

            <rect x="0." y="-0.5" width="3" height="4" fill="#242424" stroke="#000000" stroke-width="0.1" />
            <circle cx="0.5" cy="0" r="0.3" fill="#161616" />
        </g>

        <g id="wire">
            <circle cx="0%" cy="0%" r="0.175" fill="grey" stroke="none" />
            <circle cx="100%" cy="100%" r="0.175" fill="grey" stroke="none" />
            <line x1="0%" x2="100%" y1="0%" y2="100%" stroke="grey" stroke-width="0.3"/>
        </g>

        <g id="dwire">    
            <use href="#wire" />
            <line x1="8%" x2="92%" y1="8%" y2="92%" stroke="#000000" stroke-width="0.7"/>
            <line x1="10%" x2="90%" y1="10%" y2="90%" stroke-width="0.5"/>
        </g>

        <g id="dwireAd">    
            <use href="#dwire" transform="scale(1,-1)" />
        </g>

        <g id="resistor">    
            <use href="#wire" />
            <line x1="0%" x2="50%" y1="0%" y2="50%" stroke="none" stroke-width="0.5" marker-end="url(#resistorMarker)"/>
        </g>

        <g id="resistorAd">
            <use href="#resistor" transform="scale(1,-1)" />
        </g>

        <g id="capacitor">
            <use href="#wire" />
            <line x1="0%" x2="50%" y1="0%" y2="50%" stroke="none" stroke-width="0.5" marker-end="url(#capacitorMarker)"/>
        </g>

        <g id="capacitorAd">
            <use href="#capacitor" transform="scale(1,-1)" />
        </g>

    </defs>

    <rect width="{self.width + 2}" height="{self.height + 2}" fill="{self.board_color}" />

    <g transform="translate(1,1)">

    <rect width="{self.width}" height="{self.height}" fill="url(#padPattern)" />

    <g transform="translate(-0.5,-0.5)">
    <g transform="translate(2, -1)">

<!-- Generated elements -->

{self.elements}
<!-- End of generated elements -->

    </g>
    </g>
    </g>

</svg>
        """
    
    def add_element(self, x, y, w, h, element, color = None, rotate = 0):

        if rotate != 0:
            transform = f' transform="rotate({rotate})"'
        else:
            transform = ''

        if color is None:
            stroke = ''
        else:
            stroke = f' stroke="{color}"'

        if h < 0:
            self.elements += f'<svg x="{x}" y="{y}" width="{w}" height="{-h}"{transform}{stroke}> <use href="#{element}Ad"/> </svg>\n'
        else:
            self.elements += f'<svg x="{x}" y="{y}" width="{w}" height="{h}"{transform}{stroke}> <use href="#{element}"/> </svg>\n'

    def resistor(self, x, y, w, h):
        self.add_element(x, y, w, h, 'resistor')

    def capacitor(self, x, y, w, h):
        self.add_element(x, y, w, h, 'capacitor')

    def wire(self, x, y, w, h, color = None):
        self.add_element(x, y, w, h, 'dwire', color=color)

    def dip8(self, x, y, rotate=0):
        self.add_element(x, y, 3, 4, 'dip8', rotate=rotate)

    def save(self, filepath):
        with open(filepath, 'w') as file:
            file.write(self.generate_svg())